import { Component, OnInit } from '@angular/core'
import { CoursesService } from '../courses.service'
import { AutoGrowDirective } from '../auto-grow.directive'

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  providers: [CoursesService],
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  constructor(courseService:CoursesService) {
    this.courses = courseService.getCourses();
  }
  ngOnInit() {
  }
 title = "The Title of Courses Page";
 courses;
 addCourse(newCourse: string) {
    if (newCourse) {
      this.courses.push(newCourse);
    }
  }
} 
